pluginManagement {
    repositories {
        maven("https://dl.bintray.com/kotlin/kotlin-eap")

        mavenCentral()

        maven("https://plugins.gradle.org/m2/")
    }
}
enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "PidroReborn"

