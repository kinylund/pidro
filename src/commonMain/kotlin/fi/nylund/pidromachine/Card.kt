package fi.nylund.pidromachine

enum class Team(val teamName: String) {
    US("us"),
    THEM("them"),
    NONE("none")
}

enum class Suit(val suitName: String, val short: String) {
    HEARTS("Hearts", "H"),
    SPADES("Spades", "S"),
    DIAMONDS("Diamonds", "D"),
    CLUBS("Clubs", "C"),
    NONE("No card", "")
}

enum class Card(val num: Int, val suit: Suit) {
    NOCARD(0, Suit.NONE),
    HA(14, Suit.HEARTS),
    H2(2, Suit.HEARTS),
    H3(3, Suit.HEARTS),
    H4(4, Suit.HEARTS),
    H5(5, Suit.HEARTS),
    H6(6, Suit.HEARTS),
    H7(7, Suit.HEARTS),
    H8(8, Suit.HEARTS),
    H9(9, Suit.HEARTS),
    H10(10, Suit.HEARTS),
    HN(11, Suit.HEARTS),
    HQ(12, Suit.HEARTS),
    HK(13, Suit.HEARTS),
    CA(14, Suit.CLUBS),
    C2(2, Suit.CLUBS),
    C3(3, Suit.CLUBS),
    C4(4, Suit.CLUBS),
    C5(5, Suit.CLUBS),
    C6(6, Suit.CLUBS),
    C7(7, Suit.CLUBS),
    C8(8, Suit.CLUBS),
    C9(9, Suit.CLUBS),
    C10(10, Suit.CLUBS),
    CN(11, Suit.CLUBS),
    CQ(12, Suit.CLUBS),
    CK(13, Suit.CLUBS),
    SA(14, Suit.SPADES),
    S2(2, Suit.SPADES),
    S3(3, Suit.SPADES),
    S4(4, Suit.SPADES),
    S5(5, Suit.SPADES),
    S6(6, Suit.SPADES),
    S7(7, Suit.SPADES),
    S8(8, Suit.SPADES),
    S9(9, Suit.SPADES),
    S10(10, Suit.SPADES),
    SN(11, Suit.SPADES),
    SQ(12, Suit.SPADES),
    SK(13, Suit.SPADES),
    DA(14, Suit.DIAMONDS),
    D2(2, Suit.DIAMONDS),
    D3(3, Suit.DIAMONDS),
    D4(4, Suit.DIAMONDS),
    D5(5, Suit.DIAMONDS),
    D6(6, Suit.DIAMONDS),
    D7(7, Suit.DIAMONDS),
    D8(8, Suit.DIAMONDS),
    D9(9, Suit.DIAMONDS),
    D10(10, Suit.DIAMONDS),
    DN(11, Suit.DIAMONDS),
    DQ(12, Suit.DIAMONDS),
    DK(13, Suit.DIAMONDS);

    companion object {
        fun fromNum(value: Int) = values().first { it.ordinal == value }
    }
}

class CardData(val card: Card) {
    companion object
}

fun Suit.complementarySuit(): Suit {
    return when (this) {
        Suit.HEARTS ->
            Suit.DIAMONDS
        Suit.SPADES ->
            Suit.CLUBS
        Suit.CLUBS ->
            Suit.SPADES
        Suit.DIAMONDS ->
            Suit.HEARTS
        else -> Suit.NONE
    }
}

/**
 * Calculate the points for us and them this round
 */
fun List<Card>.trickPoints(): Triple<Int, Int, Int> {
    check(this.size == 4) { "This list must contain all players" }
    var us = 0
    var them = 0
    var indexOfMax = 0
    var max = 0
    val points: Int = this.foldIndexed(0) { i, acc, card ->
        if (card.num > max) {
            indexOfMax = i
            max = card.num
        }
        if (card.num == 2) {
            if (i.rem(2) == 0)
                us++ else them++
            acc
        } else acc + card.value()
    }
    return if (indexOfMax.rem(2) == 0) {
        Triple(us + points, them, indexOfMax)
    } else Triple(us, them + points, indexOfMax)

}

/**
 * Take cards playable in this suit
 */
fun List<Card>.suitCards(suit: Suit): List<Card> =
    this.filter { it.suit == suit || it.num == 5 && suit.complementarySuit() == it.suit }

/**
 * Get the card value in the game
 */
fun Card.value(): Int {
    return when (this.num) {
        14, 11, 10, 2 -> 1
        5 -> 5
        else -> 0
    }
}

/**
 * Get the card usability in the game
 */
fun Card.usability(): Int {
    return when (this.num) {
        14, 11, 10 -> 1
        5 -> 5
        else -> 0
    }
}