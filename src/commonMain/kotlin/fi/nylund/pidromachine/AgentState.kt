package fi.nylund.pidromachine

import kotlinx.serialization.Serializable

@Serializable
data class AgentState(private val agentIndex: Int) {
    var initialHand: List<Card> = listOf()
    var bids: List<Int> = List(4) { 0 }
        set(value) {
            /*val bidFields = listOf(bid0, bid1, bid2, bid3)
            bid0 = value[(agentIndex + 0).rem(4)]
            bid1 = value[(agentIndex + 1).rem(4)]
            bid2 = value[(agentIndex + 2).rem(4)]
            bid3 = value[(agentIndex + 3).rem(4)]*/
            field = value
        }
    var drawnCards: List<Int> = List(4) { 0 }
    var bidWinIndex: Int = -1
    var bidWinSuit: Suit = Suit.NONE
    var bestBid: List<Int> = List(10) { 0 }
    var bestSuit: List<Int> = List(4) { 0 }
    var playedCards: List<List<Card>> = List(4) { List(6) { Card.NOCARD } }
}