package fi.nylund.pidromachine

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class SimpleAgent(private val agentIndex: Int) : Agent {
    private val friendIndex = (agentIndex + 2).rem(4)
    private val opponentIndexes = listOf((agentIndex + 1).rem(4), (agentIndex + 3).rem(4))
    private var bestSuit: Suit = Suit.HEARTS
    private var highestBid = 0
    private var allStates: List<AgentState> = listOf()
    private var agentState: AgentState = AgentState(agentIndex)

    override fun playTurnInTrick(tricks: List<MutableList<Card>>, agentCards: Set<Card>, startIndex: Int): Card {
        if (agentCards.isEmpty()) {
            return Card.NOCARD
        }
        if (agentCards.size == 1) {
            return agentCards.first()
        }
        val agentCardsSorted by lazy { agentCards.sortedBy { it.num }.reversed() }
        val valueSorted by lazy { agentCardsSorted.sortedBy { -it.usability() } }
        val agentTurnNumber = abs(startIndex - agentIndex) + 1
        val trickRound = tricks.size
        val playedCards = tricks.subList(0, tricks.size - 1).flatten()
        val unplayedHighCards by lazy {
            val playedCardsNum = playedCards.map { it.num }
            (14 downTo 2).filterNot { it in playedCardsNum }
        }
        val friendCard by lazy { tricks[trickRound - 1][friendIndex] }
        val oponentCards: List<Card> by lazy { tricks[trickRound - 1].slice(opponentIndexes) }

        //If I am last and
        if ((startIndex + 3).rem(4) == agentIndex) {
            if (min(friendCard.num, 4) > oponentCards.maxByOrNull { it.num }?.num ?: 0) {
                agentCards.find { it.num == 5 }?.also {
                    return it
                }
            }
        }

        //When friend has high card
        if (friendCard.num == unplayedHighCards.filterNot { card -> card in agentCards.map { it.num } }.firstOrNull()) {
            return agentCardsSorted.reversed().find { it.num in listOf(5, 11, 10) } ?: valueSorted.last()
        }

        // When opponent has high card
        val nextHighCard = unplayedHighCards.filterNot { card -> card in agentCards.map { it.num } }.firstOrNull()
        if (oponentCards.find { it.num == nextHighCard } != null) {
            return valueSorted.last()
        }

        when (trickRound - 1 + agentCards.size) {
            5, 6, 7, 8, 9 -> {
                agentCards.find { it.num == unplayedHighCards.first() }?.also {
                    return it
                }
                return agentCardsSorted.find { it.num in listOf(13, 12, 9, 8, 7, 6) } ?: valueSorted.last()
            }
        }

        when (agentTurnNumber) {
            1, 2 -> {
                agentCards.find { it.num == unplayedHighCards.first() }?.also {
                    return it
                }
                agentCards.find { it.num == 5 && agentCards.size < 4 }?.also {
                    return it
                }
                return valueSorted.last()
            }
        }


        return valueSorted.last()
    }

    override fun pickSuit(): Suit {
        return if (bestSuit != Suit.NONE) bestSuit else Suit.HEARTS
    }

    override fun takeBid(hand: List<Card>, bids: List<Int?>): Int {
        agentState = AgentState(agentIndex)
        bestSuit = Suit.HEARTS
        agentState.initialHand = hand
        highestBid = 0
        for (s in 1 until 4) {
            val suit = Suit.values().first { it.ordinal == s }
            val bid = getBid(hand, suit)
            if (bid > highestBid) {
                highestBid = bid
                bestSuit = suit
            }
        }
         bestSuit
        highestBid = if (bids.filterNotNull().count() == 3 && highestBid > 0) {
            6
        } else if (bids.filterNotNull().count() == 3 &&
            bids.filterNotNull().maxOrNull() ?: 0 == highestBid
        ) {
            highestBid + 1
        } else highestBid

        agentState.bestSuit = MutableList(4){0}.apply {
            this[bestSuit.ordinal] = 1
        }.toList()
        agentState.bestBid = MutableList(10){0}.apply {
            this[max((highestBid-5),0)] = 1
        }.toList()
        return highestBid
    }

    override fun setGameState(bids: List<Int>, drawnCards: List<Int>, bidWinnerIndex: Int, suit: Suit) {
        val agentState = AgentState(agentIndex)
        agentState.bids = bids
        agentState.drawnCards = drawnCards
        agentState.bidWinIndex = bidWinnerIndex
        agentState.bidWinSuit = suit

        allStates += agentState
        FileWriter("simpleAgentAll").writeBid( agentState )
    }

    companion object {
        fun getBid(hand: List<Card>, suit: Suit): Int {
            val suitedHand = hand.suitCards(suit).toSet()
            return when {
                suitedHand.filter { it.num == 5 }.size > 1 && suitedHand.size < 4 -> {
                    0
                }
                suitedHand.filter { it.num == 5 }.size > 1 && suitedHand.size > 5 -> {
                    var bid = 10
                    bid += if (suitedHand.any { it.num == 2 }) 1 else 0
                    bid
                }
                suitedHand.any { it.num == 14 } -> {
                    var bid = 6
                    bid += if (suitedHand.any { it.num == 2 }) 1 else 0
                    bid += if (suitedHand.any { it.num in listOf(12, 13) }) 1 else 0
                    bid += if (suitedHand.size > 4 && suitedHand.any { it.num in listOf(10, 11) }) 1 else 0
                    bid += if (suitedHand.size > 4 && suitedHand.any { it.num == 13 }) 5 else 0
                    bid
                }

                else -> 0
            }
        }
    }
}