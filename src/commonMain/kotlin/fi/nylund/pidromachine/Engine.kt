package fi.nylund.pidromachine

import kotlin.math.abs
import kotlin.math.min
import kotlin.math.max
import kotlin.random.Random

class Engine(number: Int) {
    private var deck: List<Card>
    private var shufflerIndex = Random.nextInt(0, 4)

    init {
        println("Lets play ($number), player $shufflerIndex is dealer")
        deck = cardDeck.shuffled()
    }

    private fun nextDealer() {
        shufflerIndex = (shufflerIndex + 1).rem(4)
    }

    private fun deal(): List<List<Card>> {
        deck = cardDeck.shuffled()
        return List(4) {
            val hand = deck.subList(0, 9)
            deck = deck.subList(9, deck.size)
            hand
        }
    }

    private fun takeSuitAndBuy(
        hands: List<List<Card>>,
        suit: Suit,
        shufflerIndex: Int
    ): List<Pair<List<Card>, Int>> {
        var deck = deck
        val filteredHands = hands.map { hand ->
            hand.suitCards(suit)
        }
        val finalHands = MutableList<Pair<List<Card>, Int>>(4) { Pair(listOf(), 0) }
        for (i in 0 until 4) {
            val playerIndex = (i + shufflerIndex).rem(4)
            val hand = filteredHands[playerIndex]
            val want = if (i != 3) {
                min(abs(6 - hand.size), deck.size)
            } else deck.size
            val newHand = hand + deck.subList(0, want)
            deck = deck.subList(want, deck.size)
            finalHands[playerIndex] = Pair(newHand, want)
        }
        return finalHands
    }

    /**
     * Do the bidding
     */
    private fun takeAgentBids(agents: List<Agent>, hands: List<List<Card>>): List<Int> {
        val bids: MutableList<Int?> = MutableList(4) { 0 }
        for (i in 0 until 4) {
            val agentIndex = (i + shufflerIndex + 1).rem(4)
            val agentBid = agents[agentIndex].takeBid(hands[agentIndex], bids.toList())
            bids[agentIndex] =
                if (agentBid > bids.filterNotNull().maxOrNull() ?: 0)
                    agentBid
                //Last player must bid 6 if noone else bids
                else if (i == 3 && bids.filterNotNull().maxOrNull() ?: 0 == 0) {
                    6
                } else 0
        }

        return bids.filterNotNull().toList()
    }

    private fun pickSuit(agent: Agent): Suit {
        return agent.pickSuit()
    }

    private fun playRoundAgents(
        agents: List<Agent>,
        hands: List<Pair<List<Card>, Int>>,
        suit: Suit,
        bidWinnerIndex: Int
    ): List<MutableList<Card>> {
        val filteredHands = hands.map { it.first.suitCards(suit).toMutableSet() }
        val biggestList = filteredHands.maxByOrNull { it.size }?.size ?: 0
        val tricks: List<MutableList<Card>> = List(biggestList) { MutableList(4) { Card.NOCARD } }
        var startingAgent = bidWinnerIndex
        for (trickIndex in 0 until biggestList) {
            for (j in 0 until 4) {
                val agent = (startingAgent + j).rem(4)
                val playedCard: Card =
                    agents[agent].playTurnInTrick(
                        tricks.subList(0, trickIndex + 1),
                        filteredHands[agent],
                        startingAgent
                    )
                if (filteredHands[agent].remove(playedCard)) {
                    tricks[trickIndex][agent] = playedCard
                }
            }
            val trickPoints = tricks[trickIndex].trickPoints()
            startingAgent = trickPoints.third
        }
        return tricks
    }

    private fun playerHasCards(
        i: Int, filteredHands: List<List<Card>>, player: Int
    ) = i < filteredHands[player].size

    fun playWithAgents(agents: List<Agent>): List<Int> {
        val hands = deal()
        val bids = takeAgentBids(agents, hands)
        //printHands(hands, bids)
        val bidWinnerIndex = bids.withIndex().filter { it.value > 5 }.maxByOrNull { it.value }?.index ?: shufflerIndex
        val suit = pickSuit(agents[bidWinnerIndex])
        val hand = takeSuitAndBuy(hands, suit, shufflerIndex)
        agents.forEach { agent ->
            agent.setGameState(bids, drawnCards = hand.map { it.second }, bidWinnerIndex = bidWinnerIndex, suit = suit)
        }
        //printHandsBought(hand)
        val rounds = playRoundAgents(agents, hand, suit, bidWinnerIndex)
        val score = getScore(rounds)
        val winningBid = max(bids[bidWinnerIndex], 6)
        nextDealer()
        return if (score[bidWinnerIndex.rem(2)] >= winningBid) {
            score + bidWinnerIndex + bids[bidWinnerIndex]
        } else {
            score.toMutableList().apply {
                this[bidWinnerIndex.rem(2)] = -winningBid
            } + winningBid + bids[bidWinnerIndex]
        }
    }

    companion object {
        val cardDeck = (1..52).map { num -> Card.fromNum(num) }.also {
            val sb = StringBuilder("All cards in deck: ")
            println(it.joinTo(sb, separator = "','"))
        }


        fun getWinnerTeam(gameScore: List<Int>, lastBidderTeamIndex: Int): Pair<String, Int> {
            return when {
                gameScore[0] >= 62 && gameScore[1] >= 62 && lastBidderTeamIndex.rem(2) == 0 -> {
                    Pair("Us", 0)
                }
                gameScore[0] >= 62 && gameScore[1] >= 62 && lastBidderTeamIndex.rem(2) == 1 -> {
                    Pair("Them", 1)
                }
                gameScore[0] > gameScore[1] -> {
                    Pair("Us", 0)
                }
                else -> {
                    Pair("Them", 1)
                }
            }
        }

        fun printHandsBought(hands: List<Pair<List<Card>, Int>>) {
            hands.forEachIndexed { index, hand ->
                print("Player $index: (${hand.second}) -> ")
                println(hand.first.joinToString {
                    it.name
                })
            }
        }

        private fun printHands(hands: List<List<Card>>, bids: List<Int>) {
            hands.forEachIndexed { index, player ->
                println("Player $index: bid(${bids[index]})")
                /*println(player.joinToString {
                    it.name
                })*/
            }
        }

        private fun getScore(rounds: List<List<Card>>): List<Int> {
            val score = mutableListOf(0, 0)
            rounds.forEachIndexed { index, round ->
                /*print("Trick ${index + 1}: ")
                print(round.joinToString("\t") {
                    if (it != Card.NOCARD) "${it.name}" else "* "
                })*/
                val points = round.trickPoints()
                score[0] += points.first
                score[1] += points.second
                //println("\tus: ${score[0]} \tthem: ${score[1]}")
            }
            return score.toList()
        }
    }
}
