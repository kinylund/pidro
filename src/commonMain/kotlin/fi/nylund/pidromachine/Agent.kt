package fi.nylund.pidromachine

interface Agent {
    /**
     * Play a card from this agent returning the card played
     */
    fun playTurnInTrick(tricks: List<MutableList<Card>>, agentCards: Set<Card>, startIndex: Int): Card
    fun takeBid(hand: List<Card>, bids: List<Int?>): Int
    fun pickSuit(): Suit
    fun setGameState(bids: List<Int>, drawnCards: List<Int>, bidWinnerIndex: Int, suit: Suit)
}