package fi.nylund.pidromachine

expect class FileWriter(fileName: String) {
    fun writeBid(agentState: AgentState)
    fun writeBids(agentState: Collection<AgentState>)
}