package fi.nylund.pidromachine


import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis
import kotlin.math.max

internal object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        println("execution took " + measureTimeMillis {
            val numGames = 5000
            val chunked: Int = (numGames / 50) + 1
            val wins: MutableList<Int> = mutableListOf(0, 0)
            val bids: List<MutableList<Int>> = MutableList(9) { mutableListOf(0, 0) }
            var plumpCount = List(2) { 0 }
            val totalRounds =
                (1..numGames).chunked(chunked).pmap { iterator ->
                    iterator.sumOf { gameNum ->
                        val game = Engine(gameNum)

                        var agentIndex = 0
                        val agents: List<Agent> = listOf(
                            SimpleAgent(agentIndex++),
                            SimpleAgent(agentIndex++),
                            SimpleAgent(agentIndex++),
                            SimpleAgent(agentIndex)
                        )

                        var gameScore = List(3) { 0 }
                        var plumpCountGame = List(2) { 0 }
                        var roundsCounter = 0
                        var lastBidderTeam = 0
                        while (gameScore.subList(0, 2).maxOrNull() ?: 0 < 62) {
                            val roundScore = game.playWithAgents(agents)
                            gameScore = gameScore.zip(roundScore) { tot, trick -> tot + trick }
                            lastBidderTeam = roundScore[2]
                            plumpCountGame = checkPlump(plumpCountGame, roundScore)
                            synchronized(bids) {
                                bids[roundScore[3] - 6][0]++
                                if (roundScore.subList(0, 2).minOrNull() ?: 0 < 0) {
                                    bids[roundScore[3] - 6][1]++
                                }
                            }

                            roundsCounter++
                        }
                        val winner =
                            Engine.getWinnerTeam(gameScore.subList(0, 2), lastBidderTeam)
                        synchronized(wins) {
                            wins[winner.second]++
                            plumpCount = plumpCount.zip(plumpCountGame) { tot, game -> tot + game }
                        }
                        println("\n Game over in $roundsCounter rounds \n final score \tUs: ${gameScore[0]} \tThem: ${gameScore[1]} ($lastBidderTeam)")
                        println(" Congratulations to team: \"$winner\" in game($gameNum)")
                        roundsCounter
                    }
                }.sum()

            println(
                "\n\nFinal game score is Us: ${wins[0]} and Them: ${wins[1]} \n" +
                        "Team Us plumeted: ${plumpCount[0]} and Them: ${plumpCount[1]}"
            )
            (0 until 9).forEach {
                println("Bid ${it + 6}:\t ${bids[it][0]}\t (${bids[it][1]}) \t${100 - (bids[it][1] * 100) / max(bids[it][0],1)}%")
            }
            println("Total rounds of $totalRounds played with an average of ${totalRounds / numGames} rounds per game")
        } + "ms")
    }

    private fun checkPlump(plumpCount: List<Int>, roundScore: List<Int>): List<Int> {
        return plumpCount.zip(roundScore.subList(0, 2)) { plump, points ->
            if (points < 0) plump + 1 else plump
        }
    }

}

fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B): List<B> =
    runBlocking {
        map { async(Dispatchers.Default) { f(it) } }.map { it.await() }
    }
