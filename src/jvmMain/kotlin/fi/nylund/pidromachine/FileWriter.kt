package fi.nylund.pidromachine

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import fi.nylund.pidromachine.AgentState

actual class FileWriter actual constructor(val fileName: String) {

    actual fun writeBid(agentState: AgentState) {
        File(fileName).appendText(
            "${json.encodeToString(agentState)}\n"
        )
    }

    actual fun writeBids(agentState: Collection<AgentState>) {

    }

    companion object {
        val json = Json { }
    }

}